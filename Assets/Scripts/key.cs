﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class key : MonoBehaviour
{
    public GameObject door;

    private void OnTriggerEnter(Collider collide)
    {
        if (collide.gameObject.name == "Player")
        {
            Destroy(door.gameObject);
            Destroy(gameObject);
        }

    }


}
