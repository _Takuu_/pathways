﻿using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Rigidbody pm;
    public float speed = 0f;
    public float xforce = 10f;
    

    void FixedUpdate()
    {
        if (Input.GetKey("w"))
        {
            pm.AddForce(-xforce* Time.deltaTime, 0, 0,ForceMode.VelocityChange);
        }
        if (Input.GetKey("s"))
        {
            pm.AddForce(xforce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        }
        if (Input.GetKey("d"))
        {
            pm.AddForce(0, 0,xforce * Time.deltaTime, ForceMode.VelocityChange);
        }
        if (Input.GetKey("a"))
        {
            pm.AddForce(0, 0,-xforce * Time.deltaTime, ForceMode.VelocityChange);
        }

    }

}
